import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    subtitleView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: 5
    },
    sellerText: {
      resizeMode: 'contain',
      color: 'white',
      fontWeight: 'bold'
    },
    description: {
      flexDirection: 'column',
      marginRight:10,
      paddingRight:5,
      color: 'white'
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
  export default styles;  