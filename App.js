import {Platform,FlatList, StyleSheet, Text, View} from 'react-native'
import {createStackNavigator, createAppContainer} from 'react-navigation';
import { ListItem, Header } from 'react-native-elements'
  import Communities from './src/Communities';
import CommunityDetails from './src/CommunityDetails';

const MainNavigation = createStackNavigator({
  Communities: { screen: Communities },
  CommunityDetails: { screen: CommunityDetails }
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
 }
);
const App = createAppContainer(MainNavigation);

export default App;