
import React, {Component} from 'react';
import {Platform,FlatList, StyleSheet, Dimensions, Text, View , ListView} from 'react-native';
import { ListItem, Header , SearchBar } from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale'; 
import styles from './Styles'
import LinearGradient from 'react-native-linear-gradient'; 
import Server from './Server';

const title = "COMMUNITIES"
var {height, width} = Dimensions.get('window');
type Props = {};
export default class Communities extends Component<Props> {
    state = {
        loading: false,      
        data: [],
        arrayholder:[],      
        error: null,  
        Query: "",  
      };

  

  componentDidMount(){
      let options = {
        "type": "GET",
        "url" : '/interview/api/communities/all',
        }
        Server.getData(options, function (response) {
         console.log('RESPONSE' + JSON.stringify(response)); 
         this.setState({
           data : response.communities || [],
           arrayholder : response.communities || []
         })
         
          }.bind(this))
  }

  keyExtractor = (item, index) => index.toString()
  searchFilterFunction = text => {    
      this.setState({
          query:text
      })
    const newData = this.state.arrayholder.filter(item => {      
      const itemData = `${item.name.toUpperCase()}   
      ${item.name.toUpperCase()} ${item.name.toUpperCase()}`;
       const textData = text.toUpperCase();
       return itemData.indexOf(textData) > -1;    
    });
    
    this.setState({ data: newData });  
  };

  renderHeader = () => {    
    return (      
      <SearchBar    
        width = {width}  
        placeholder="Type Here..."        
        lightTheme        
        round        
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
        value={this.state.query}             
      />    
    );  
  };
  renderItem = ({ item }) => (
    <ListItem
    onPress={() => this.props.navigation.navigate('CommunityDetails' , {
      id: item._id || '',
    })}
    style = {{marginTop:10}}
    containerStyle = {{borderBottomColor:'red', borderRadius:20}}
    Component={TouchableScale}
    friction={90} //
    tension={100} // These props are passed to the parent component (here TouchableScale)
    activeScale={0.95} //
    linearGradientProps={{
      colors: ['#0575DE', '#61A2DF'],
      start: [1, 0],
      end: [0.2, 0],
    }}
    ViewComponent={LinearGradient} // Only if no expo
    leftAvatar={{ rounded: true, source: { uri: item.photo || 'https://miro.medium.com/max/1120/1*iYqaWhPjjjuigQ3BV12RRQ.jpeg' } }}
    title= {item.name}
    titleStyle={{ color: 'white', fontWeight: 'bold' }}
    subtitleStyle={{ color: 'white' }}
    subtitle = {
      <View style={styles.subtitleView}>
        <Text style={styles.description}>{item.description}</Text>
        <Text style={styles.sellerText}>{item.sellers.length||'0'}  {(item.sellers.length <= 1) ? 'Seller' : 'Sellers'} </Text>
      </View>
    }
    chevronColor="white"
    chevron
  />
  )

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
      <Header
      leftComponent={{ icon: 'menu', color: '#fff' }}
      centerComponent={{ text: title, style: { color: '#fff' } }}
      rightComponent={{ icon: 'home', color: '#fff' }}
    />
    
   
  <FlatList          
    style = {{width:width-20 , height:height , marginLeft:10, marginRight:10}}
    data={this.state.data}          
    renderItem={this.renderItem}          
    keyExtractor={this.keyExtractor}  
    ListHeaderComponent={this.renderHeader}                             
  />            




</View>
    );
  }
}

