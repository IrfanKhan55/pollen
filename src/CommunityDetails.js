import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { ListItem, Header } from 'react-native-elements'
import { WebView } from 'react-native-webview';
const title = "COMMUNITY DETAILS"
type Props = {};
export default class CommunityDetails extends Component<Props>  {

    render() {
      const {navigate} = this.props.navigation;

      const communityID = this.props.navigation.getParam('id', '5cebf1f8cd9a0909871f7b8c'); // default ID in second parameter
      console.log('ID = '+ communityID)
       
      return (
        <View style={{flex: 1}}>
        <Header
        leftComponent={{ icon: 'arrow-back', color: '#fff' , onPress: () => this.props.navigation.goBack() }}
        centerComponent={{ text: title, style: { color: '#fff' } }}
        rightComponent={{ icon: 'home', color: '#fff' }}
      />
      <WebView
       startInLoadingState={true}
       source={{uri: 'https://community-staging.pollenstores.co/community/'+communityID+'/h'}}
       style={{flex: 1}}
  />
      </View>
      );
    }
  }